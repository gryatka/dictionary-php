<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200822203827 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE languages_services (language_id UUID NOT NULL, service_id UUID NOT NULL, PRIMARY KEY(language_id, service_id))');
        $this->addSql('CREATE INDEX IDX_CEF33CBD82F1BAF4 ON languages_services (language_id)');
        $this->addSql('CREATE INDEX IDX_CEF33CBDED5CA9E6 ON languages_services (service_id)');
        $this->addSql('CREATE TABLE services (service_id UUID NOT NULL, service_name VARCHAR(255) NOT NULL, PRIMARY KEY(service_id))');
        $this->addSql('ALTER TABLE languages_services ADD CONSTRAINT FK_CEF33CBD82F1BAF4 FOREIGN KEY (language_id) REFERENCES languages (language_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE languages_services ADD CONSTRAINT FK_CEF33CBDED5CA9E6 FOREIGN KEY (service_id) REFERENCES services (service_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE languages_services DROP CONSTRAINT FK_CEF33CBDED5CA9E6');
        $this->addSql('DROP TABLE languages_services');
        $this->addSql('DROP TABLE services');
    }
}
