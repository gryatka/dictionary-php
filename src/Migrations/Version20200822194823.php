<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200822194823 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE languages (language_id UUID NOT NULL, language_name VARCHAR(255) NOT NULL, PRIMARY KEY(language_id))');
        $this->addSql('CREATE TABLE lessons (lesson_id UUID NOT NULL, language_id UUID NOT NULL, lesson_name VARCHAR(255) NOT NULL, lesson_text TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(lesson_id))');
        $this->addSql('CREATE INDEX IDX_3F4218D982F1BAF4 ON lessons (language_id)');
        $this->addSql('ALTER TABLE lessons ADD CONSTRAINT FK_3F4218D982F1BAF4 FOREIGN KEY (language_id) REFERENCES languages (language_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE lessons DROP CONSTRAINT FK_3F4218D982F1BAF4');
        $this->addSql('DROP TABLE languages');
        $this->addSql('DROP TABLE lessons');
    }
}
