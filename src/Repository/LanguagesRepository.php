<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class LanguagesRepository
 * @package App\Repository
 */
class LanguagesRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function findAll()
    {
        return $this->findBy([], array('created_at' => 'ASC'));
    }
}
