<?php

namespace App\Services\API;

use App\Services\Core\BaseService;
use App\Entity\Languages;
use App\Entity\Services;

/**
 * Class LessonsService
 * @package App\Services
 */
class ApiLanguagesService extends BaseService
{
    /**
     * @return array
     */
    public function getLanguages(): array
    {
        return $this->em->getRepository(Languages::class)->findAll();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function saveLanguageChanges(array $data): bool
    {
        if ($this->hasKeyBody($data)) {
            $body = $data['body'];

            foreach ($body as $item) {
                /**
                 * @var Languages $language
                 */
                $language = $this->em->getRepository(Languages::class)
                    ->findOneBy(['language_id' => $item['language_id']]);
                $this->setMainFields($language, $item);
                $this->setServices($item);
            }
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * @param Languages $language
     * @param array $data
     */
    private function setMainFields(Languages $language, array $data): void
    {
        $language->setLanguageName($data['language_name']);
        $language->setLanguageDescription($data['language_description']);
        $language->setLanguagePath($data['language_path']);
        $language->setLanguageShortname($data['language_shortname']);
        $this->em->persist($language);
    }

    /**
     * @param array $data
     */
    private function setServices(array $data): void
    {
        if (array_key_exists('services', $data)) {
            foreach ($data['services'] as $service) {
                $serviceRepo = $this->em->getRepository(Services::class)
                    ->findOneBy(['service_id' => $service['service_id']]);
                $serviceRepo->setServiceName($service['service_name']);
                $this->em->persist($serviceRepo);
            }
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function addLanguage(array $data): bool
    {
        if ($this->hasKeyBody($data)) {
            $language = new Languages();
            $this->setMainFields($language, $data['body']);
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function deleteLanguage(string $id): bool
    {
        $repoLanguage = $this->em->getRepository(Languages::class)
            ->findOneBy(['language_id' => $id]);
        if (null != $repoLanguage) {
            $this->em->remove($repoLanguage);
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * @param string $lang
     * @param array $data
     * @return bool
     */
    public function addService(string $lang, array $data): bool
    {
        if ($this->hasKeyBody($data)) {
            /**
             * @var Languages $repoLanguage
             */
            $repoLanguage = $this->em->getRepository(Languages::class)
                ->findOneBy(['language_shortname' => $lang]);
            $service = new Services();
            $service->setServiceName($data['body']['service_name']);
            $service->setServicePath($data['body']['service_path']);
            $service->addLanguage($repoLanguage);

            $this->em->persist($service);
            $this->em->flush();
            return true;
        }
        return false;
    }

    /**
     * @param string $lang
     * @param string $serviceId
     * @return bool
     */
    public function deleteService(string $lang, string $serviceId): bool
    {
        $language = $this->em->getRepository(Languages::class)
            ->findOneBy(['language_shortname' => $lang]);
        $service = $this->em->getRepository(Services::class)
            ->findOneBy(['service_id' => $serviceId]);
        if (null !== $language && null !== $service) {
            $service->removeLanguage($language);

            $this->em->persist($service);
            $this->em->remove($service);
            $this->em->flush();
            return true;
        }
        return false;
    }
}
