<?php

namespace App\Services\API;

use App\Services\Core\BaseService;
use App\Entity\Languages;
use App\Entity\Lessons;

/**
 * Class LessonsService
 * @package App\Services
 */
class ApiLessonsService extends BaseService
{
    /**
     * @param string $lang
     * @return array
     */
    public function getLessons(string $lang): array
    {
        $repoLanguage = $this->em->getRepository(Languages::class)
            ->findOneBy(['language_shortname' => $lang]);
        return $this->em->getRepository(Lessons::class)->findBy(['language' => $repoLanguage], ['created_at' => 'ASC']);
    }

    /**
     * @param string $lang
     * @param string $link
     * @return ?Lessons
     */
    public function getLesson(string $lang, string $link): ?Lessons
    {
        $repoLanguage = $this->em->getRepository(Languages::class)
            ->findOneBy(['language_shortname' => $lang]);
        return $this->em->getRepository(Lessons::class)
            ->findOneBy(['language' => $repoLanguage, 'lesson_path' => $link]);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function addLesson(array $data): bool
    {
        if ($this->hasKeyBody($data)) {
            $repoLanguage = $this->em->getRepository(Languages::class)
                ->findOneBy(['language_shortname' => $data['body']['lang']]);

            if (null != $repoLanguage) {
                $lesson = new Lessons();
                $this->setMainFields($lesson, $data['body']);
                $lesson->setLanguage($repoLanguage);

                $this->em->persist($lesson);
                $this->em->flush();

                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param Lessons $lesson
     * @param array $data
     */
    private function setMainFields(Lessons $lesson, array $data): void
    {
        $lesson->setLessonName($data['lesson_name']);
        $lesson->setLessonText($data['lesson_text']);
        $lesson->setLessonPath($data['lesson_path']);
        $this->em->persist($lesson);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function editLesson(array $data): bool
    {
        if ($this->hasKeyBody($data)) {
            $repoLesson = $this->em->getRepository(Lessons::class)
                ->findOneBy(['lesson_id' => $data['body']['lesson_id']]);
            if (null != $repoLesson) {
                $this->setMainFields($repoLesson, $data['body']);

                $this->em->persist($repoLesson);
                $this->em->flush();

                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function deleteLesson(string $id): bool
    {
        $repoLesson = $this->em->getRepository(Lessons::class)
            ->findOneBy(['lesson_id' => $id]);
        if (null != $repoLesson) {
            $this->em->remove($repoLesson);
            $this->em->flush();
            return true;
        }
        return false;
    }
}
