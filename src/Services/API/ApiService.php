<?php

namespace App\Services\API;

use App\Services\Core\BaseService;

/**
 * Class ApiService
 * @package App\Services
 */
class ApiService extends BaseService
{
    /**
     * @return string[]
     */
    public function getSiteInfo(): array
    {
        return [
            'name' => 'SiteName',
            'description' => 'Quickly build an effective pricing table for your potential customers with this 
                Bootstrap example. It\'s built with default Bootstrap components and utilities with 
                little customization.'
        ];
    }
}
