<?php

namespace App\Services\API;

use App\Entity\User;
use App\Services\Core\BaseService;

/**
 * Class UserService
 * @package App\Services
 */
class ApiUserService extends BaseService
{
    /**
     * @return array
     */
    public function getCurrentUser(): array
    {
        $user = $this->user;
        if ('anon.' != $user && null != $user) {
            return [
                'username' => $user->getUsername(),
                'roles' => $user->getRoles(),
            ];
        } else return [
            'username' => '',
            'roles' => [""],
        ];
    }

    /**
     * @param array $data
     * @return string
     */
    public function addRoleAdmin(array $data): string
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['username' => $data['userName']]);
        if ($user) {
            if (!$user->hasRole('ROLE_ADMIN')) {
                $user->addRole('ROLE_ADMIN');
                $this->em->persist($user);
                $this->em->flush();
                return 'role added';
            }
            return 'has role';
        }
        return 'not user';
    }

    /**
     * @param array $data
     * @return string
     */
    public function removeRoleAdmin(array $data): string
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['username' => $data['userName']]);
        if ($user) {
            if ($user->hasRole('ROLE_ADMIN')) {
                $user->removeRole('ROLE_ADMIN');
                $this->em->persist($user);
                $this->em->flush();
                return 'role removed';
            }
            return 'has not role';
        }
        return 'not user';
    }
}
