<?php

namespace App\Services\Core;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class BaseService
 * @package App\Services
 */
abstract class BaseService
{
    protected $user;
    protected $em;

    /**
     * BaseService constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $em
     */
    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $em)
    {
        if ($tokenStorage->getToken() !== null) {
            $this->user = $tokenStorage->getToken()->getUser();
        }
        $this->em = $em;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function hasKeyBody(array $data): bool
    {
        if (array_key_exists('body', $data)) {
            return true;
        }
        return false;
    }
}
