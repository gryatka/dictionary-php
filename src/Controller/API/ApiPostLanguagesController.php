<?php

namespace App\Controller\API;

use App\Controller\Core\BaseController;
use App\Services\API\ApiLanguagesService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiPostLanguagesController
 * @package App\Controller
 * @Route("/api/post/languages", methods={"POST"})
 */
class ApiPostLanguagesController extends BaseController
{
    /**
     * @Route(
     *     "/addLanguage",
     *      name="api_post_languages_add_language",
     * )
     * @param ApiLanguagesService $service
     * @param Request $request
     * @return JsonResponse
     */
    public function addLanguage(ApiLanguagesService $service, Request $request): JsonResponse
    {
        return new JsonResponse($service->addLanguage(self::decodeRequest($request)));
    }

    /**
     * @Route(
     *     "/deleteLanguage/{id}",
     *      name="api_post_languages_delete_language",
     * )
     * @param ApiLanguagesService $service
     * @param string $id
     * @return JsonResponse
     */
    public function deleteLanguage(ApiLanguagesService $service, string $id): JsonResponse
    {
        return new JsonResponse($service->deleteLanguage($id));
    }

    /**
     * @Route(
     *     "/addService/{lang}",
     *      name="api_post_languages_add_service",
     * )
     * @param ApiLanguagesService $service
     * @param string $lang
     * @param Request $request
     * @return JsonResponse
     */
    public function addService(ApiLanguagesService $service, string $lang, Request $request): JsonResponse
    {
        return new JsonResponse($service->addService($lang,self::decodeRequest($request)));
    }

    /**
     * @Route(
     *     "/deleteService/{lang}/{id}",
     *      name="api_post_languages_delete_service",
     * )
     * @param ApiLanguagesService $service
     * @param string $lang
     * @param string $id
     * @return JsonResponse
     */
    public function deleteService(ApiLanguagesService $service, string $lang, string $id): JsonResponse
    {
        return new JsonResponse($service->deleteService($lang, $id));
    }

    /**
     * @Route(
     *     "/saveLanguageChanges",
     *      name="api_post_languages_save_language_changes",
     * )
     * @param ApiLanguagesService $service
     * @param Request $request
     * @return JsonResponse
     */
    public function saveLanguageChanges(ApiLanguagesService $service, Request $request): JsonResponse
    {
        return new JsonResponse($service->saveLanguageChanges(self::decodeRequest($request)));
    }
}
