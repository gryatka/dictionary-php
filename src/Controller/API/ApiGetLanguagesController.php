<?php

namespace App\Controller\API;

use App\Controller\Core\BaseController;
use App\Services\API\ApiLanguagesService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiGetLanguagesController
 * @package App\Controller
 * @Route("/api/get/languages", methods={"GET"})
 */
class ApiGetLanguagesController extends BaseController
{
    /**
     * @Route(
     *     "/receiveLanguages/",
     *      name="api_lessons_receive_languages"
     * )
     * @param ApiLanguagesService $service
     * @return Response
     */
    public function receiveLanguages(ApiLanguagesService $service): Response
    {
        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($service->getLanguages(), 'json');
        return new Response($json, 200);
    }
}
