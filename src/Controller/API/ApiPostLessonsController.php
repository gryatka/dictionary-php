<?php

namespace App\Controller\API;

use App\Controller\Core\BaseController;
use App\Services\API\ApiLessonsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiPostLessonsController
 * @package App\Controller
 * @Route("/api/post/lessons", methods={"POST"})
 */
class ApiPostLessonsController extends BaseController
{
    /**
     * @Route(
     *     "/addLesson",
     *      name="api_post_lessons_add_lesson",
     * )
     * @param ApiLessonsService $service
     * @param Request $request
     * @return JsonResponse
     */
    public function addLesson(ApiLessonsService $service, Request $request): JsonResponse
    {
        return new JsonResponse($service->addLesson(self::decodeRequest($request)));
    }

    /**
     * @Route(
     *     "/editLesson",
     *      name="api_post_lessons_edit_lesson",
     * )
     * @param ApiLessonsService $service
     * @param Request $request
     * @return JsonResponse
     */
    public function editLesson(ApiLessonsService $service, Request $request): JsonResponse
    {
        return new JsonResponse($service->editLesson(self::decodeRequest($request)));
    }

    /**
     * @Route(
     *     "/deleteLesson/{id}",
     *      name="api_post_lessons_delete_lesson",
     * )
     * @param ApiLessonsService $service
     * @param string $id
     * @return JsonResponse
     */
    public function deleteLesson(ApiLessonsService $service, string $id): JsonResponse
    {
        return new JsonResponse($service->deleteLesson($id));
    }
}
