<?php

namespace App\Controller\API;

use App\Controller\Core\BaseController;
use App\Services\API\ApiUserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 * @package App\Controller
 * @Route("/api/user")
 */
class ApiUserController extends BaseController
{
    /**
     * @Route(
     *     "/currentUser/",
     *      name="api_user_receive_currentuser"
     * )
     * @param ApiUserService $service
     * @return JsonResponse
     */
    public function currentUser(ApiUserService $service): JsonResponse
    {
        return new JsonResponse($service->getCurrentUser());
    }

    /**
     * @Route(
     *     "/addRoleAdmin",
     *      name="api_user_addroleadmin",
     *     methods={"POST"}
     * )
     *
     * @param ApiUserService $service
     * @param Request $request
     * @return JsonResponse
     */
    public function addRoleAdmin(ApiUserService $service, Request $request): JsonResponse
    {
        return new JsonResponse($service->addRoleAdmin(self::decodeRequest($request)));
    }

    /**
     * @Route(
     *     "/removeRoleAdmin",
     *      name="api_user_removeroleadmin"
     * )
     * @param ApiUserService $service
     * @param Request $request
     * @return JsonResponse
     */
    public function removeRoleAdmin(ApiUserService $service, Request $request): JsonResponse
    {
        return new JsonResponse($service->removeRoleAdmin(self::decodeRequest($request)));
    }
}
