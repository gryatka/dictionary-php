<?php

namespace App\Controller\API;

use App\Controller\Core\BaseController;
use App\Services\API\ApiService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 * @package App\Controller
 * @Route("/api")
 */
class ApiController extends BaseController
{
    /**
     * @Route(
     *     "/receiveSiteInfo/",
     *      name="api_receive_siteinfo"
     * )
     * @param ApiService $service
     * @return Response
     */
    public function receiveSiteInfo(ApiService $service): Response
    {
        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($service->getSiteInfo(), 'json');
        return new Response($json, 200);
    }
}
