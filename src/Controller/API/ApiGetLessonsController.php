<?php

namespace App\Controller\API;

use App\Controller\Core\BaseController;
use App\Services\API\ApiLessonsService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiGetLessonsController
 * @package App\Controller
 * @Route("/api/get/lessons", methods={"GET"})
 */
class ApiGetLessonsController extends BaseController
{
    /**
     * @Route(
     *     "/receiveLessons/{lang}",
     *      name="api_get_lessons_receive_lessons",
     *      requirements={"lang"="[A-z]+"}
     * )
     * @param ApiLessonsService $service
     * @param string $lang
     * @return Response
     */
    public function receiveLessons(ApiLessonsService $service, string $lang): Response
    {
        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($service->getLessons($lang), 'json');
        return new Response($json, 200);
    }

    /**
     * @Route(
     *     "/receiveLesson/{lang}/{link}",
     *      name="api_get_lessons_receive_lesson",
     *      requirements={"lang"="[A-z]+", "link"="\w+"}
     * )
     * @param ApiLessonsService $service
     * @param string $lang
     * @param string $link
     * @return Response
     */
    public function receiveLesson(ApiLessonsService $service, string $lang, string $link): Response
    {
        $lesson = $service->getLesson($lang, $link);
        if (!is_null($lesson)) {
            $serializer = $this->get('jms_serializer');
            $json = $serializer->serialize($service->getLesson($lang, $link), 'json');
            return new Response($json, 200);
        } else throw new NotFoundHttpException($link . ' not found');
    }
}
