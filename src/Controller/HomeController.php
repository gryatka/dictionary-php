<?php

namespace App\Controller;

use App\Controller\Core\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends BaseController
{
    /**
     * @Route(
     *     "/{vueRouting}",
     *     requirements={
     *         "vueRouting"="^(?!api|_(profiler|wdt)|account).*"},
     *     name="home"
     * )
     * @return Response
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }
}
