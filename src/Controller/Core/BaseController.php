<?php

namespace App\Controller\Core;

use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BaseController
 * @package App\Controller
 */
abstract class BaseController extends AbstractController
{
    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), ['jms_serializer' => '?' . SerializerInterface::class,]);
    }
    /**
     * @param Request $request
     * @return array
     */
    protected static function decodeRequest(Request $request): array
    {
        $content = $request->getContent();
        if ($content) {
            return json_decode($content, true);
        } return [];
    }
}
