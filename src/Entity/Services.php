<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ServicesRepository")
 * @ORM\Table(name="services")
 */
class Services
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="guid", name="service_id")
     */
    private $service_id;

    /**
     * @var string
     * @ORM\Column(type="string", name="service_name")
     */
    private $service_name;

    /**
     * @var string
     * @ORM\Column(type="string", name="service_path")
     */
    private $service_path;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $created_at;

    /**
     * @ORM\ManyToMany(targetEntity="Languages", mappedBy="services")
     */
    private $languages;

    /**
     * Languages constructor.
     */
    public function __construct()
    {
        $this->service_id = Uuid::uuid4();
        $this->languages = new ArrayCollection();
        $this->created_at = new DateTime();
    }

    /**
     * @return string
     */
    public function getServiceId(): string
    {
        return $this->service_id;
    }

    /**
     * @return string
     */
    public function getServiceName(): string
    {
        return $this->service_name;
    }

    /**
     * @param string $service_name
     */
    public function setServiceName(string $service_name): void
    {
        $this->service_name = $service_name;
    }

    /**
     * @return string
     */
    public function getServicePath(): string
    {
        return $this->service_path;
    }

    /**
     * @param string $service_path
     */
    public function setServicePath(string $service_path): void
    {
        $this->service_path = $service_path;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->created_at;
    }

    /**
     * @return ArrayCollection
     */
    public function getLanguages(): ArrayCollection
    {
        return $this->languages;
    }

    /**
     * @param Languages $language
     * @return $this
     */
    public function addLanguage(Languages $language): self
    {
        if (!$this->languages->contains($language)) {
            $this->languages[] = $language;
            $language->addService($this);
        }
        return $this;
    }

    /**
     * @param Languages $language
     * @return $this
     */
    public function removeLanguage(Languages $language): self
    {
        if ($this->languages->contains($language)) {
            $this->languages->removeElement($language);
            $language->removeService($this);
        }
        return $this;
    }
}
