<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\LanguagesRepository")
 * @ORM\Table(name="languages")
 */
class Languages
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="guid", name="language_id")
     */
    private $language_id;

    /**
     * @var string
     * @ORM\Column(type="string", name="language_name")
     */
    private $language_name;

    /**
     * @var string
     * @ORM\Column(type="string", name="language_description")
     */
    private $language_description;

    /**
     * @var string
     * @ORM\Column(type="string", name="language_path")
     */
    private $language_path;

    /**
     * @var string
     * @ORM\Column(type="string", name="language_shortname")
     */
    private $language_shortname;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity="Lessons", mappedBy="language")
     */
    private $lessons;

    /**
     * @ORM\ManyToMany(targetEntity="Services", inversedBy="languages")
     * @OrderBy({"created_at" = "ASC"})
     * @ORM\JoinTable(
     *  name="languages_services",
     *  joinColumns={
     *      @ORM\JoinColumn(name="language_id", referencedColumnName="language_id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="service_id", referencedColumnName="service_id")
     *  }
     * )
     */
    private $services;

    /**
     * Languages constructor.
     */
    public function __construct()
    {
        $this->language_id = Uuid::uuid4();
        $this->lessons = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->created_at = new DateTime();
    }

    /**
     * @return string
     */
    public function getLanguageId(): string
    {
        return $this->language_id;
    }

    /**
     * @return string
     */
    public function getLanguageName(): string
    {
        return $this->language_name;
    }

    /**
     * @param string $language_name
     */
    public function setLanguageName(string $language_name): void
    {
        $this->language_name = $language_name;
    }

    /**
     * @return string
     */
    public function getLanguageDescription(): string
    {
        return $this->language_description;
    }

    /**
     * @param string $language_description
     */
    public function setLanguageDescription(string $language_description): void
    {
        $this->language_description = $language_description;
    }

    /**
     * @return string
     */
    public function getLanguagePath(): string
    {
        return $this->language_path;
    }

    /**
     * @param string $language_path
     */
    public function setLanguagePath(string $language_path): void
    {
        $this->language_path = $language_path;
    }

    /**
     * @return string
     */
    public function getLanguageShortname(): string
    {
        return $this->language_shortname;
    }

    /**
     * @param string $language_shortname
     */
    public function setLanguageShortname(string $language_shortname): void
    {
        $this->language_shortname = $language_shortname;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->created_at;
    }

    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * @param Lessons $lesson
     * @return $this
     */
    public function addLesson(Lessons $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
        }
        return $this;
    }

    /**
     * @param Lessons $lesson
     * @return $this
     */
    public function removeLesson(Lessons $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
        }
        return $this;
    }

    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param Services $service
     * @return $this
     */
    public function addService(Services $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->addLanguage($this);
        }
        return $this;
    }

    /**
     * @param Services $service
     * @return $this
     */
    public function removeService(Services $service): self
    {
        if ($this->services->contains($service)) {
            $this->services->removeElement($service);
            $service->removeLanguage($this);
        }
        return $this;
    }

    /**
     * @param Services $services
     * @return $this
     */
    public function removeServices(Services $services): self
    {
        foreach ($services as $service) {
            if ($this->services->contains($service)) {
                $this->services->removeElement($service);
            }
        }
        return $this;
    }
}
