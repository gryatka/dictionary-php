<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\LessonsRepository")
 * @ORM\Table(name="lessons")
 */
class Lessons
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="guid", name="lesson_id")
     */
    private $lesson_id;

    /**
     * @var string
     * @ORM\Column(type="string", name="lesson_name")
     */
    private $lesson_name;

    /**
     * @var string
     * @ORM\Column(type="text", name="lesson_text")
     */
    private $lesson_text;

    /**
     * @var string
     * @ORM\Column(type="string", name="lesson_path")
     */
    private $lesson_path;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="Languages", inversedBy="lessons")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="language_id", nullable=false)
     */
    private $language;


    /**
     * JapanLessons constructor.
     */
    public function __construct()
    {
        $this->lesson_id = Uuid::uuid4();
        $this->created_at = new DateTime();
    }

    /**
     * @return string
     */
    public function getLessonId(): string
    {
        return $this->lesson_id;
    }

    /**
     * @return string
     */
    public function getLessonName(): string
    {
        return $this->lesson_name;
    }

    /**
     * @param string $lesson_name
     */
    public function setLessonName(string $lesson_name): void
    {
        $this->lesson_name = $lesson_name;
    }

    /**
     * @return string
     */
    public function getLessonText(): string
    {
        return $this->lesson_text;
    }

    /**
     * @param string $lesson_text
     */
    public function setLessonText(string $lesson_text): void
    {
        $this->lesson_text = $lesson_text;
    }

    /**
     * @return string
     */
    public function getLessonPath(): string
    {
        return $this->lesson_path;
    }

    /**
     * @param string $lesson_path
     */
    public function setLessonPath(string $lesson_path): void
    {
        $this->lesson_path = $lesson_path;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }
}
