<?php

namespace App\Tests\Services\API;

use App\Services\API\ApiService;
use App\Services\Core\BaseService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class BaseServiceTest
 * @package App\Tests\Services\Core
 */
class ApiServiceTest extends TestCase
{
    /**
     * @var MockObject|BaseService
     */
    private $service = null;

    protected function setUp()
    {
        /**
         * @var TokenStorageInterface|MockObject $token
         */
        $token = $this->getMockBuilder(TokenStorageInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        /**
         * @var EntityManagerInterface|MockObject $entityManager
         */
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->service = new ApiService($token, $entityManager);
    }

    protected function tearDown()
    {
        $this->service = null;
    }

    public function testGetSiteInfo()
    {
        $result = $this->service->getSiteInfo();
        $this->assertIsArray($result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('description', $result);
    }
}