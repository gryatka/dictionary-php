<?php

namespace App\Tests\Services\Core;

use App\Services\Core\BaseService;
use App\Services\Tests\Helper;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

use Doctrine\ORM\EntityManagerInterface;
use ReflectionException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class BaseServiceTest
 * @package App\Tests\Services\Core
 */
class BaseServiceTest extends TestCase
{
    /**
     * @var MockObject|BaseService
     */
    private $service = null;

    protected function setUp()
    {
        /**
         * @var TokenStorageInterface|MockObject $token
         */
        $token = $this->getMockBuilder(TokenStorageInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        /**
         * @var EntityManagerInterface|MockObject $entityManager
         */
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->service = $this->getMockForAbstractClass(
            BaseService::class,
            [$token, $entityManager]
        );
    }

    protected function tearDown()
    {
        $this->service = null;
    }

    public function testBaseService()
    {
        $this->assertClassHasAttribute('user', BaseService::class);
        $this->assertClassHasAttribute('em', BaseService::class);
    }

    /**
     * @dataProvider additionProvider
     * @param $data
     * @param $expected
     * @throws ReflectionException
     */
    public function testHasKeyBody($data, $expected)
    {
        $result = Helper::invokeMethod($this->service, 'hasKeyBody', $data);
        $this->assertSame($expected, $result);
    }

    public function additionProvider()
    {
        return [
            'has key Body' => [['body' => 'value'], true],
            'has not key Body' => [['key' => 'value'], false],
        ];
    }
}