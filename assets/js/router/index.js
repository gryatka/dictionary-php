import Vue from "vue";
import VueRouter from "vue-router";

import IndexPage from "../views/core/IndexPage";
import Lessons from "../views/lessons/Lessons";
import Lesson from "../views/lessons/Lesson";
import NotFound from "../views/core/NotFound";
import PersonalPage from "../views/account/PersonalPage";

import AdminIndexPage from "../views/admin/AdminIndexPage";
import AdminRolesPage from "../views/admin/AdminRolesPage";
import AdminLanguagesPage from "../views/admin/AdminLanguagesPage";

Vue.use(VueRouter);

export default new VueRouter({
    mode: "history",
    routes: [
        { path: '/', component: IndexPage },

        { caseSensitive: true, path: '/account/*' },
        { caseSensitive: true, path: '/personalpage/', component: PersonalPage },

        { path: '/:lang/lessons', component: Lessons },
        { path: '/:lang/lessons/:link', component: Lesson },

        { caseSensitive: true, path: '/admin/', component: AdminIndexPage },
        { caseSensitive: true, path: '/admin/roles', component: AdminRolesPage },
        { caseSensitive: true, path: '/admin/languages', component: AdminLanguagesPage },

        { path: '*', component: NotFound }
    ]
});
