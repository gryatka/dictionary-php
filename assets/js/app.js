import Vue from "vue";
import App from "./App";
import router from "./router";

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

import MainHeader from "./components/core/MainHeader";
import SiteInfo from "./components/core/SiteInfo";
import LanguagesList from "./components/core/LanguagesList";
import LessonsList from "./components/lessons/LessonsList";
import LessonsListItem from "./components/lessons/LessonsListItem";
import LessonsListItemAdminBtn from "./components/lessons/LessonsListItemAdminBtn";
import AdminLanguagesList from "./components/admin/languages/AdminLanguagesList";
import AdminLanguagesListItem from "./components/admin/languages/AdminLanguagesListItem";
import AdminLanguagesAdd from "./components/admin/languages/AdminLanguagesAdd";

Vue.component('main-header', MainHeader);
Vue.component('site-info', SiteInfo);
Vue.component('languages-list', LanguagesList);
Vue.component('lessons-list', LessonsList);
Vue.component('lessons-list-item', LessonsListItem);
Vue.component('lessons-list-item-admin-btn', LessonsListItemAdminBtn);
Vue.component('admin-languages-list', AdminLanguagesList);
Vue.component('admin-languages-list-item', AdminLanguagesListItem);
Vue.component('admin-languages-add', AdminLanguagesAdd);

Vue.prototype.$hostname = 'http://localtest.main/api';

new Vue({
    components: { App },
    template: "<App/>",
    router
}).$mount("#app");
