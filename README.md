## Project specifications
## Requirements
* PHP 7.4.10
* Symfony 4.4.1
* PostgreSQL 12.4
* Vue 2.6.12
* Auth by FOSUserBundle

## Deploy project
#### Download project
* git clone ..

#### Install composer dependencies
* composer install

#### Install db
* php bin/console doctrine:schema:update

#### Install yarn dependencies
* yarn install

#### Build Vue js
* yarn encore dev

#### Tests
* php bin/phpunit
